const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

// put all sheets into constants
const htmlBundleConfigs = ['blotter', 'dma', 'summary'].map((item) => {
  return new HtmlWebpackPlugin({
    filename: `${item}/index.html`,
    template: 'public/index.html',
    title: item.charAt(0).toUpperCase() + item.slice(1),
    chunks: [item],
    inject: true,
  });
});

module.exports = {
  mode: 'development',
  entry: {
    // context: path.resolve(__dirname, './src/sheets'),
    blotter: './src/sheets/blotter.js',
    dma: './src/sheets/dma.js',
    summary: './src/sheets/summary.js',
  },
  output: {
    filename: '[name]/[name].bundle.js',
    path: path.resolve(__dirname, 'dist'),
  },
  resolve: {
    extensions: ['js', 'jsx'],
  },
  plugins: [new CleanWebpackPlugin(), ...htmlBundleConfigs],
};
