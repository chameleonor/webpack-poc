import express from 'express';
import path from 'path';

const app = express();
app.use(express.json());
app.use(express.static(path.join(__dirname, 'src')));
app.use(express.static(path.join(__dirname, 'dist')));

/* serves main page  */
app.get('/blotter', (req, res) =>
  res.sendFile('dist/blotter/index.html', { root: __dirname })
);

app.listen(3000, () => {
  console.log('listen on port 3000');
});
